export interface RPersonaje {
    data: Personaje[];
    meta: Meta;
}

export interface Personaje {
    idel?:number,
    id:number,
    img?: string,
    name: string
}

export interface Meta {
    count: number;
}