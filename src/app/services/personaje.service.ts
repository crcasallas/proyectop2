import { Injectable } from '@angular/core';
import { Personaje, RPersonaje } from '../interface/personaje.interface';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonajeService {

  
 
  myList: Personaje[] = [];
  myCart = new BehaviorSubject<Personaje[]>([]);
  myCart$ = this.myCart.asObservable();
  //DATOS POR PERSONAJE
  listaUnidadS:{}={}
  myLU = new BehaviorSubject<{}>({});
  myLU$ = this.myLU.asObservable(); 
  
  indice!:number;

  constructor(private http: HttpClient) { }

  getPersonajeApi (filtro:string, limit:number, offset:number){    
    if(filtro===''){
      return this.http.get<RPersonaje>(`${environment.urlChar}?page[limit]=${limit}&page[offset]=${offset}`)
    }else{
      return this.http.get<RPersonaje>(`${environment.urlChar}?filter[name]=${filtro}?page[limit]=${limit}&page[offset]=${offset}`)
    }
  }

  getPersonajeApiUni (ide:number){
    return this.http.get<RPersonaje>(`${environment.urlChar}/${ide}`)
  }

  filtrarLista(filtro:string){
    return this.http.get<RPersonaje>(`${environment.urlChar}?filter[name]=${filtro}`)
  }

  agregarALista(personaje:Personaje){
    this.myList.push(personaje);
    this.myCart.next(this.myList);    
  }

  eliminarDeLista(i:number){        
    delete this.myList[i];
    this.myList.splice(i,1);    
    this.myCart.next(this.myList);       
  }

  verPersonaje(i:number){ 
    this.indice = i;               
    const listaUnidad: Personaje ={
      id: this.myList[i].id,
      idel: this.myList[i].idel,
      img: this.myList[i].img,
      name: this.myList[i].name
    }
    this.listaUnidadS=listaUnidad;
    this.myLU.next(this.listaUnidadS);
  }

  guardarLista (i:number, personaje:Personaje){
    i = this.indice;
    this.myList.splice(i,1,personaje);
    this.myCart.next(this.myList);
  }
}
