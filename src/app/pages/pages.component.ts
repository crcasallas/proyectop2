import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Menu } from '../interface/menu.interface';
import { MenuService } from '../services/menu.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  @ViewChild(MatSidenav) sidenav!: MatSidenav;

  menu: Menu[] = [];
  showFiller = false;

  constructor(private menuService: MenuService, private router:Router, private observer:BreakpointObserver) {
    this.cargarMenu();
   }

   ngAfterViewInit(){

    setTimeout(()=>{
      this.observer.observe(['(max-width: 800px)']).subscribe(resp=>{
        if(resp.matches){
          this.sidenav.mode= 'over';
          this.sidenav.close();
        }else{
         this.sidenav.mode= 'side';
         this.sidenav.open();
        }
      })
    },0)

   }

  ngOnInit(): void {    
  }

  cargarMenu(){
    this.menuService.getMenu().subscribe(resp => {
      this.menu = resp;
    })
  }

  salir(){
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

}
