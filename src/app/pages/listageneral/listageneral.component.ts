import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Personaje, RPersonaje } from 'src/app/interface/personaje.interface';
import { PersonajeService } from 'src/app/services/personaje.service'; 
@Component({
  selector: 'app-listageneral',
  templateUrl: './listageneral.component.html',
  styleUrls: ['./listageneral.component.scss']
})
export class ListageneralComponent implements OnInit {

  info:any={};
  infoAlmacenada:any={};  
  limit: number = 5;
  offset: number = 0;
  total!: number;
  filtro:string='';
  displayedColumns: string[] = ['id','img', 'personaje', 'agregar'];  
  //dataSource = new MatTableDataSource<any>([]);//tambien funciona
  dataSource !: MatTableDataSource<Personaje>;//funciona pero da un error de paginador undefined si se coloca el paginator en ngAfterViewInit
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;  

  constructor(private personajeService: PersonajeService, private _snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.cargarPersonaje();
  }

  cargarPersonaje(){
    this.personajeService.getPersonajeApi(this.filtro, this.limit, this.offset).subscribe(resp =>{
      this.dataSource = new MatTableDataSource(resp.data.slice())
      this.total=resp.meta.count;
      this.dataSource.paginator = this.paginator;           
    })    
  }

  OnPageActivated(event:any){
    this.limit=event.pageSize;
    this.offset=(Number(event.pageIndex))* Number(event.pageSize);    
    this.personajeService.getPersonajeApi(this.filtro, this.limit,this.offset).subscribe(resp=>{
      this.dataSource = new MatTableDataSource(resp.data.slice())
      this.total=resp.meta.count;
    })    
  }

  seleccionar(ide:number){
    this.personajeService.getPersonajeApiUni(ide)
    .subscribe((resp:RPersonaje)=>{
      this.info= resp;      
      if (typeof(Storage)!=='undefined'){      
        localStorage.setItem("infou", JSON.stringify(this.info));
        this.infoAlmacenada=JSON.parse(localStorage.getItem("infou")!);        
        const infoListaPersonal:Personaje ={
          id:this.infoAlmacenada.data.id,
          img:this.infoAlmacenada.data.attributes?.image?.tiny,
          name:this.infoAlmacenada.data.attributes.name        
        }
        this.personajeService.agregarALista(infoListaPersonal);        
      }else{
        alert("storage no es compatible en este navegador")
      }    
    });    
    this._snackBar.open('Pesonaje agregado a la lista personal', '', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
         
  } 

  applyFilter($event: any) {
    //this.dataSource.filter = $event.target.value;
    const filterValue =this.camelToUnderscore(($event.target as HTMLInputElement).value);    
    this.dataSource.filter = filterValue.trim()
    this.filtro=filterValue;
    if (this.dataSource.filter===''){
      this.cargarPersonaje();
    }else{
      this.personajeService.filtrarLista(this.dataSource.filter).subscribe(resp =>{
         this.dataSource = new MatTableDataSource(resp.data.slice())         
         this.total=resp.meta.count;
         this.dataSource.paginator = this.paginator;         
       })        
    }    
  }

  camelToUnderscore(key:any) {
    var result = key.replace( /([A-Z])/g, " $1" );
    return result.split(' ').join('%20').toLowerCase();
 }
}
