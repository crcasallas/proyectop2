import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Personaje } from 'src/app/interface/personaje.interface';
import { PersonajeService } from 'src/app/services/personaje.service'; 


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  unidad:any = {};
  listaunidad:any ={};
  myLU$:any;  
  idel:any={};
  dataSource!:MatTableDataSource<Personaje>;
  formEditar: FormGroup = this.fb.group({
    img: ['',Validators.required],
    name: ['',Validators.required]
  })  

  constructor(private fb:FormBuilder, private personajeService:PersonajeService) {
    this.editar(this.idel);
   }

  ngOnInit(): void {
  }

  editar(idel:number){
    this.myLU$=this.personajeService.myLU$.subscribe( resp =>{
      this.listaunidad=resp;           
      const listado:Personaje = {
        id: this.listaunidad.id,
        idel: this.listaunidad.idel,
        img: this.listaunidad.img,
        name: this.listaunidad.name        
      }      
      this.unidad=listado;      
   }) 
  }

  guardarLista(i:number, id:number){     
    const personaje:Personaje = {
      id:id, 
      idel:i,
      img: this.formEditar.value.img,
      name: this.formEditar.value.name      
    }
    this.personajeService.guardarLista(i, personaje);    
  }

  ngOnDestroy(): void{
    this.myLU$.unsubscribe();
  }

}
